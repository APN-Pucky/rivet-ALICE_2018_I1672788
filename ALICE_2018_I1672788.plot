BEGIN PLOT /ALICE_2018_I1672788/.*
LegendAlign=r
MainPlot=1
RatioPlot=1
END PLOT


BEGIN PLOT /ALICE_2018_I1672788/d01-x01-y01
Title=The dielectron cross section in inelastic pp collisions at $\sqrt s = 13$ TeV as a function of invariant mass for $p_{\mathrm{T},\ell\ell}  < 6.0$ GeV/$c$
XLabel=$m_{\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} m_{\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672788/d02-x01-y01
Title=The dielectron cross section in inelastic pp collisions at $\sqrt s = 13$ TeV as a function of pair transverse momentum for $m_{\ell\ell} < 0.14$ GeV/$c^2$
XLabel=$p_{T,\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672788/d03-x01-y01
Title=The dielectron cross section in inelastic pp collisions at $\sqrt s = 13$ TeV as a function of pair transverse momentum for $0.14< m_{\ell\ell} < 0.7$ GeV/$c^2$
XLabel=$p_{T,\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672788/d04-x01-y01
Title=The dielectron cross section in inelastic pp collisions at $\sqrt s = 13$ TeV as a function of pair transverse momentum for $0.7< m_{\ell\ell} < 1.03$ GeV/$c^2$
XLabel=$p_{T,\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672788/d10-x01-y01
Title=The dielectron cross section in inelastic pp collisions at $\sqrt s = 13$ TeV as a function of pair transverse momentum for $1.03 <  m_{\ell,\ell} < 2.86$ GeV/$c^2$
XLabel=$p_{T,\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672788/mass_*
Title=Dilepton invariant mass spectrum
XLabel=$m_{\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} m_{\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT